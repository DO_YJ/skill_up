#include <stdio.h>

int main(void)
{
	int x = 100;
	printf("현재 x의 값은%d입니다.\n", x);
	x += 50; // x = x + 50;
	printf("현재 x의 값은%d입니다.\n", x);
	x -= 50; // x = x - 50;
	printf("현재 x의 값은%d입니다.\n", x);
	x *= 50; // x = x * 50;
	printf("현재 x의 값은%d입니다.\n", x);
	x /= 50; // x = x / 50;
	printf("현재 x의 값은%d입니다.\n", x);
	x %= 3; // 자신을 나눈 3으로 값으로 돌아감. 100을3으로 나누고 남은게 1이니까 1이됨. 
	printf("현재 x의 값은%d입니다.\n", x);
	return 0;
}




